package XRakutenAPI.TestRunner.PUTEmployee;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
//import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(Cucumber.class)
//@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "/home/amocanu/Documents/intelij/Diabolocom/rakutenx/src/test/java/XRakutenAPI/features/editEmployee.feature"
        ,glue={"XRakutenAPI.TestRunner.PUTEmployee"}
        ,format= {"pretty","html:test-output"}
)

public class PutEmployeeTest {
}
