package XRakutenAPI.TestRunner.PUTEmployee;

import XRakutenAPI.Actions.UpdateEmployeeActions.EditEmployee;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

public class UpdateEmployee {

    Response response=null;
    String itemUpdate="";
    EditEmployee employeeUpdate= new EditEmployee();
    String[] values={"","",""};

    @Given("^that the user performs a request to update information for an employee with a \"([^\"]*)\" id with \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
    public void that_the_user_performs_a_request_to_update_information_for_an_employee_with_a_id_with(String id, String name, String salary, String age) {
        response=employeeUpdate.performUpdateEmployee(id,name,salary,age);
        itemUpdate=id;
        values[0]=name; values[1]=salary;values[2]=age;
    }

    @When("^the result is retrieved$")
    public void the_result_is_retrieved(){
        employeeUpdate.getResponseAsString(response);
    }

    @Then("^the user can check the response code$")
    public void the_user_can_check_the_response_code(){
        employeeUpdate.checkResponseCode(itemUpdate);
    }

    @Then("^the user can validate the response format$")
    public void the_user_can_validate_the_response_format(){
        employeeUpdate.validateResponseContent();
        Assert.assertTrue("Parameters are the expected ones",employeeUpdate.respectsFormatValues(values));
    }
}
