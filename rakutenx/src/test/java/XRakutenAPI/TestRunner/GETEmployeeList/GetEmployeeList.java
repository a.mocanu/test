package XRakutenAPI.TestRunner.GETEmployeeList;

import XRakutenAPI.Actions.GetEmployeeListActions.GetList;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class GetEmployeeList {

    private GetList employeeList=new GetList();

    @Given("^that the user performs a request to retrieve the employee list$")
    public void that_the_user_performs_a_request_to_retrieve_the_employee_list() {
        employeeList.performGetEmployeeList();
    }

    @When("^the result is retrieved$")
    public void the_result_is_retrieved() {
        employeeList.getResponseAsString();
    }

    @Then("^the user can check the expected success response code$")
    public void the_user_can_check_the_response_code() {
        employeeList.checkResponseCode();
    }

    @And("^the user can validate the response format and check number of objects found$")
    public void the_user_can_validate_the_response_format(){
        employeeList.validateResponseContent();
    }

    @And("^the user can validate if all employee have a valid id$")
    public void the_user_can_validate_if_all_employee_have_a_valid_id(){
        Assert.assertTrue("The IDs and Names of the elements are not empty",employeeList.filledInIdAndValid());
    }
}
