package XRakutenAPI.TestRunner.DELETEEmployee;

import XRakutenAPI.Actions.DeleteEmployeeActions.DeleteSpecificEmployee;
import XRakutenAPI.Actions.GetEmployeeActions.GetSpecEmployee;
import XRakutenAPI.Actions.GetEmployeeListActions.GetList;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

import static XRakutenAPI.Actions.Actions.makeSomeLog;

public class DeleteEmployee {

    private DeleteSpecificEmployee removeItem=new DeleteSpecificEmployee();
    private Response response=null;
    private String deletedItemId="";
    private GetSpecEmployee employee=new GetSpecEmployee();
    private GetList employeeList=new GetList();

    @Given("^that the user performs a request to delete an employee with a \"([^\"]*)\" id$")
    public void that_the_user_performs_a_request_to_delete_an_employee_with_a_id(String id){
        response=removeItem.performDeleteEmployee(id,employeeList);
        deletedItemId=removeItem.getGeneratedId();
    }

    @When("^the result is retrieved$")
    public void the_result_is_retrieved(){
        removeItem.getResponseAsString();
    }

    @Then("^the user can check the response code$")
    public void the_user_can_check_the_response_code(){
        removeItem.checkResponseCode(deletedItemId);
    }

    @Then("^the user can validate the response format$")
    public void the_user_can_validate_the_response_format(){
        if(removeItem.validateDeleteEmployeeId(deletedItemId)){
            Assert.assertEquals("Assert that the response message for the list of employees is success",response.asString(),"{\"success\":{\"text\":\"successfully! deleted Records\"}}");
            makeSomeLog("Item successfully deleted");
        }
        else{
            Assert.assertTrue("Assert that the response message for the list of employees is error",response.asString().contains("error"));
            makeSomeLog("Item could not be deleted");
        }
    }

    @Then("^the user can check that the employee was removed from the employee list$")
    public void the_user_can_check_that_the_employee_was_removed_from_the_employee_list(){
        removeItem.checkThatItemNotInList(deletedItemId,employee);
    }
}
