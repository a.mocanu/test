package XRakutenAPI.TestRunner.POSTEmployee;

import XRakutenAPI.Actions.CreateEmployeeActions.CreateNewEmployee;
import XRakutenAPI.Actions.GetEmployeeActions.GetSpecEmployee;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

public class CreateEmployee {

    private CreateNewEmployee createEmployee=new CreateNewEmployee();
    private GetSpecEmployee getEmployee=new GetSpecEmployee();
    private Response response=null;
    private String userName,userAge,userSalary,userId="";

    @Given("^that the user performs a request to add a new employee the \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" data is used$")
    public void that_the_user_performs_a_request_to_add_a_new_employee_the_and_data_is_used(String name, String age, String salary){
        response=createEmployee.performCreateEmployee(name,age,salary);
        userName=name; userAge=age;userSalary=salary;
    }

    @When("^employee is created and is found in the employee list$")
    public void employee_is_created(){
        userId=createEmployee.getNewEmployeeId();
        boolean flag=userId.isEmpty();
        Assert.assertTrue("Assert that the response does not come in the form of an error message",flag==false);

        Response tempResponse=response;
        response=getEmployee.performGetEmployee(userId);
        getEmployee.validateResponseContent();
        response=tempResponse;
    }

    @Then("^the response code can be retrieved$")
    public void the_response_code_can_be_retrieved(){
        createEmployee.checkResponseCode();
    }

    @Then("^the response format can be validated$")
    public void the_response_format_can_be_retrieved(){
        createEmployee.validateResponseContent();
    }

    @And("^the response contains the expected values$")
    public void the_response_contains_the_expected_values(){
        Assert.assertTrue("Response values match expected values",createEmployee.validateResponseValues(userName,userAge,userSalary));

    }
    @And("^the employee is added in the employee list$")
    public void the_employee_is_added_in_the_employee_list(){
        Assert.assertNotEquals(getEmployee.performGetEmployee(userId).asString(),"false");
        Assert.assertEquals(getEmployee.performGetEmployee(userId).getStatusCode(),200);
    }

}
