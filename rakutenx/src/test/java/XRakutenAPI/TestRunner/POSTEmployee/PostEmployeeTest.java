package XRakutenAPI.TestRunner.POSTEmployee;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;



@RunWith(Cucumber.class)
@CucumberOptions(
        features = "/home/amocanu/Documents/intelij/Diabolocom/rakutenx/src/test/java/XRakutenAPI/features/createEmployee.feature"
        ,glue={"XRakutenAPI.TestRunner.POSTEmployee"}
        ,format= {"pretty","html:test-output"}
)
public class PostEmployeeTest {
}
