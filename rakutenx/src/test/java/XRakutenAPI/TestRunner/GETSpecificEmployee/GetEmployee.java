package XRakutenAPI.TestRunner.GETSpecificEmployee;

import XRakutenAPI.Actions.GetEmployeeActions.GetSpecEmployee;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import static XRakutenAPI.Actions.Actions.makeSomeLog;

public class GetEmployee {
    private String myEmployee="";
    private GetSpecEmployee employee=new GetSpecEmployee();

    @Given("^that the user performs a request to retrieve information for an employee with a \"([^\"]*)\" id$")
    public void that_the_user_performs_a_request_to_retrieve_information_for_an_employee_with_a_id(String id) {
       employee.performGetEmployee(id);
    }

    @When("^the result is retrieved$")
    public void the_result_is_retrieved() {
        myEmployee=employee.getEmployeeResponse();
    }

    @Then("^the user can check the response code$")
    public void the_user_can_check_the_response_code() {
        employee.checkResponseCode();
    }

    @And("^the user can validate the response format$")
    public void the_user_can_validate_the_response_format() {
        employee.validateResponseContent();
    }

    @And("^the user can validate that the ID is valid$")
    public void the_user_can_validate_taht_the_id_is_valid() {
        if(!myEmployee.equals("false")){
            Assert.assertTrue(employee.filledInIdAndValid());
        }
        else{
            makeSomeLog("No ID to verify");
        }

    }
}
