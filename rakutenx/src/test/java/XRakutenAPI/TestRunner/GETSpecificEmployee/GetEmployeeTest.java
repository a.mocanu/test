package XRakutenAPI.TestRunner.GETSpecificEmployee;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "/home/amocanu/Documents/intelij/Diabolocom/rakutenx/src/test/java/XRakutenAPI/features/specificEmployee.feature"
        ,glue={"XRakutenAPI.TestRunner.GETSpecificEmployee"}
        ,format= {"pretty","html:test-output"}
)

public class GetEmployeeTest {

}
