Feature: CreateEmployee

  @get_employee_list
  Scenario Outline: Add a new employee to the list and check format

    Given that the user performs a request to add a new employee the "<name>", "<salary>" and "<age>" data is used
    When employee is created and is found in the employee list
    Then the response code can be retrieved
    Then the response format can be validated
    And the employee is added in the employee list

    Examples:
      | name | salary  | age  |
      | Lalala2x9  | Xaxaxa  | Rathausstras |
      | dsofna1123168 |   |   |