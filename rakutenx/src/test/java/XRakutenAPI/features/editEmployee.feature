Feature: EmployeeUpdate

  @update_employee
  Scenario Outline: Update information for an employee

    Given that the user performs a request to update information for an employee with a "<id>" id with "<name>", "<age>", "<salary>"
    When the result is retrieved
    Then the user can check the response code
    And the user can validate the response format

    Examples:
      | id  | name  | age | salary  |
      | -5 |  haha  | 323 | kj  |
      | valid | 9982  | -5  | #$  |
      | 15595 | 6546  | something | somethingelse |
      | 15596 | helloasfdasaf3 | 52  | 2 |
      | 15597 | hello | 5 | 3 |