Feature: EmployeeList

  @get_employee_list
  Scenario Outline: Retrieve employee information and check format

    Given that the user performs a request to retrieve information for an employee with a "<id>" id
    When the result is retrieved
    Then the user can check the response code
    And the user can validate the response format
    And the user can validate that the ID is valid

    Examples:
    | id  |
    | invalid  |
    | valid |
    | 1500  |
    | asfa  |