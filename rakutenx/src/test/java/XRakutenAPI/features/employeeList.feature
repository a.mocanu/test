Feature: EmployeeList

  @get_employee_list
  Scenario: Retrieve employee list and check format

    Given that the user performs a request to retrieve the employee list
    When the result is retrieved
    Then the user can check the expected success response code
    Then the user can validate the response format and check number of objects found
    And the user can validate if all employee have a valid id
