Feature: DeleteEmployee

  @get_employee_list
  Scenario Outline: Delete a specific employee and check format

    Given that the user performs a request to delete an employee with a "<id>" id
    When the result is retrieved
    Then the user can check the response code
    Then the user can validate the response format
    Then the user can check that the employee was removed from the employee list

    Examples:
      | id  |
      | random_char  |
      | 123 |
      | # |
      | ! |
      | @ |
      | % |
      | lidl |
      | 15438 |