package XRakutenAPI.Actions.GetEmployeeListActions;

import XRakutenAPI.Actions.Actions;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Assert;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class GetList extends Actions {

    private Response response=null;

    public Response performGetEmployeeList() {
        RestAssured.baseURI = getEndpoint();
        try {
            response = RestAssured.given()
                    .when()
                    .get("/employees");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public boolean filledInIdAndValid(){
        ArrayList<Map<String,String>> jsonAsArrayList = response.jsonPath().get("");
        for(int i=0;i<jsonAsArrayList.size();i++){
            Map<String,String> currentEmployee = jsonAsArrayList.get(i);
            if(currentEmployee.get("id").isEmpty() || !isInteger(currentEmployee.get("id"))){
                makeSomeLog("ID value is empty or invalid");
                return false;
            }
        }
        return true;
    }

    public void checkResponseCode(){
        makeSomeLog("Response code for the retrieval of the employee list is :"+response.getStatusCode()+" and the expected is: 200");
        Assert.assertEquals("Assert that the response code for the list of employees is 200",response.getStatusCode(),200);
    }

    public void validateResponseContent(){
        boolean containsExpectedValues=responseHasCorrectParameters(response);
        Assert.assertTrue("Assert that the response contains the expected parameters",containsExpectedValues);
        makeSomeLog("IF response contains at least one Object, it has the following list of elements: name, age, salary, id, profile_image");
    }

    //=================================================================
    //Utility methods. Methods that help the actual tests be performed.
    //=================================================================

    public String getResponseAsString(){
        String my_response=convertToString(response);
        if(response.getContentType().equals("application/json")){
            makeSomeLog("Retrieved response is in JSON format");
        }
        return my_response;
    }

    public String getExistingId(Response response){
        String id="";
        ArrayList<Map<String,String>> jsonAsArrayList = response.jsonPath().get("");
        if(jsonAsArrayList.size()>0)
        {
            id=jsonAsArrayList.get(0).get("id");
        }
        return id;
    }

    private boolean responseHasCorrectParameters(Response response){
        String[] expectedValues = getExpectedValues();
        ArrayList<Map<String,?>> jsonAsArrayList = response.jsonPath().get("");
        Arrays.sort(expectedValues);
        int responseSize = jsonAsArrayList.size();
        makeSomeLog(responseSize+" Objects have been found in the response");

        for(int i=0;i<responseSize;i++){
            Map<String,?> currentEmployee = jsonAsArrayList.get(i);
            String[] keyList=currentEmployee.keySet().toArray(new String[currentEmployee.size()]);
            if(keyList.length!=expectedValues.length){
                makeSomeLog("Response Object does not have the expected number of parameters");
                return false;
            }
            else{
                for(int j=0;j<keyList.length;j++){
                    String key = keyList[j];
                    if (Arrays.binarySearch(expectedValues,key)==-1){
                        makeSomeLog(key + "parameter was not found in the response");
                        return false;
                    }
                    return true;
                }
            }
        }
        makeSomeLog("Response Objects contain the same number of parameters the expected response has.\n"+
                "Parameters found in response Objects match the expected parameters");
        return true;
    }
}
