package XRakutenAPI.Actions;

import io.restassured.response.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Actions {
    public static String convertToString(Response response){
        String responseAsString=response.getBody().asString();
        makeSomeLog("Response retrieved as string");
        return responseAsString;
    }

    public static String getEndpoint(){
        String endpoint="http://dummy.restapiexample.com/api/v1";
        return endpoint;
    }

    public static String[] getExpectedValues(){
        String[] expectedValues={"id","profile_image","employee_age","employee_name","employee_salary"};
        return expectedValues;
    }

    public static String[] getExpectedUpdateValues(){
        String[] expectedValues={"id","profile_image","age","name","salary"};
        return expectedValues;
    }

    private final static Logger LOGGER=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    public static void makeSomeLog(String message)
    {
        LOGGER.log(Level.INFO, message);
    }

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            return false;
        }

        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }
}
