package XRakutenAPI.Actions.UpdateEmployeeActions;

import XRakutenAPI.Actions.Actions;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import java.util.Map;

public class EditEmployee extends Actions {
    private Response response=null;

    public Response performUpdateEmployee(String id, String name, String salary, String age) {
        String body="{\n" +
                "  \"name\": \""+name+"\",\n" +
                "  \"salary\": \""+salary+"\",\n" +
                "  \"age\": \""+age+"\"\n" +
                "}";
        RestAssured.baseURI = getEndpoint();
        try {
            response = RestAssured.given()
                    .contentType(ContentType.JSON)
                    .body(body)
                    .put("/update"+"/"+id);
        } catch (Exception e) {
            makeSomeLog("Update unsuccessful");
            e.printStackTrace();
        }
        return response;
    }

    public int checkResponseCode(String itemToBeDeleted){
        boolean isIntTransformable=isInteger(itemToBeDeleted);
        if(itemToBeDeleted.equals("valid"))
        {
            isIntTransformable=true;
        }
        if(isIntTransformable){
            Assert.assertEquals("Assert that the response code for the list of employees is 200",response.getStatusCode(),200);
            makeSomeLog("Response code for the retrieval of the employee list is 200");
        }
        else{
            Assert.assertEquals("Assert that the response code for the invalid input is 400",response.getStatusCode(),400);
            makeSomeLog("Response code for the deletion of: "+itemToBeDeleted+" is: "+response.getStatusCode()+" while expected was 400");
        }
        return response.getStatusCode();
    }

    public void validateResponseContent(){
        if(response.asString().equals("false")){
            Assert.assertTrue("Assert that the response contains the expected parameters",!response.asString().contains("{"));
        }
        else{
            boolean flag=true;
            if(!getResponseAsString(response).contains("error")){
            Map<String,String> jsonResponse= response.jsonPath().get("");
            flag=respectsFormat(jsonResponse);}
            else {flag=false;}
            Assert.assertTrue("Assert that the response contains the expected parameters",flag);
            makeSomeLog("If response contains at least one Object, it has the following list of elements: name, age, salary, id, profile_image");
        }
    }

    //=================================================================
    //Utility methods. Methods that help the actual tests be performed.
    //=================================================================

    public String getResponseAsString(Response response){
        String my_response=convertToString(response);
        if(response.getContentType().equals("application/json")){
            makeSomeLog("Retrieved response is in JSON format");
        }
        return my_response;
    }

    private boolean respectsFormat(Map<String,?> jsonResponse){
        String[] expectedValues = getExpectedUpdateValues();
        for(int i=0;i<jsonResponse.size();i++){
            Map<String,?> currentEmployee = jsonResponse;
            String[] keyList=currentEmployee.keySet().toArray(new String[currentEmployee.size()]);

            if(keyList.length!=expectedValues.length-2){
                return false;
            }
            else{
                for(int j=0;j<keyList.length;j++){
                    String key = keyList[j];
                    if (!ArrayUtils.contains(expectedValues, key)){
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean respectsFormatValues(String[] expectedValues) {
        if(!getResponseAsString(response).contains("error")){
        Map<String, String> currentEmployee = response.jsonPath().get("");
        String[] keyList = currentEmployee.keySet().toArray(new String[currentEmployee.size()]);

        for (int i = 0; i < expectedValues.length; i++) {
            if (!currentEmployee.get(keyList[i]).equals(expectedValues[i])) {
                return false;
            }
        }
        }
        else{ return false;}
        return true;
    }
}
