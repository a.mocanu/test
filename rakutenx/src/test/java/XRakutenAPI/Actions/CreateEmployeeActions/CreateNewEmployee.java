package XRakutenAPI.Actions.CreateEmployeeActions;

import XRakutenAPI.Actions.Actions;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Assert;
import java.util.Map;

public class CreateNewEmployee extends Actions {

    private Response response=null;

    public Response performCreateEmployee(String name,String salary, String age) {
        String body="{\n" +
                "  \"name\": \""+name+"\",\n" +
                "  \"salary\": \""+salary+"\",\n" +
                "  \"age\": \""+age+"\"\n" +
                "}";
        RestAssured.baseURI = getEndpoint();
        try {
            response = RestAssured.given()
                    .contentType(ContentType.JSON)
                    .body(body)
                    .post("/create");
            System.out.println(response.asString());
        } catch (Exception e) {
            makeSomeLog("Employee creation failed with error");
            e.printStackTrace();
        }
        return response;
    }

    public void checkResponseCode(){
        makeSomeLog("Response code for the retrieval of the employee list is :"+response.getStatusCode()+" and the expected is: 200");
        Assert.assertEquals("Assert that the response code for the list of employees is 200",response.getStatusCode(),200);
    }

    public void validateResponseContent(){
        if(response.asString().equals("false")){
            Assert.assertTrue("Assert that the response contains the expected parameters",!response.asString().contains("{"));
        }
        else{
            Map<String,String> jsonResponse= response.jsonPath().get("");
            boolean flag=respectsFormat(jsonResponse);
            Assert.assertTrue("Assert that the response contains the expected parameters",flag);
            makeSomeLog("If response contains at least one Object, it has the following list of elements: name, age, salary, id, profile_image");
        }
    }

    public boolean validateResponseValues(String name, String age, String salary){
         Map<String,String> jsonAsArrayList = response.jsonPath().get("");
        if(jsonAsArrayList.get("id").isEmpty() || !jsonAsArrayList.get("name").equals(name) || !jsonAsArrayList.get("age").equals(age)|| !jsonAsArrayList.get("salary").equals(salary)){
            makeSomeLog("Employee values do not match the expected ones");
            return false;
        }
        makeSomeLog("All Employee values match the expected ones");
        return true;
    }

    //=================================================================
    //Utility methods. Methods that help the actual tests be performed.
    //=================================================================

    private boolean respectsFormat(Map<String,?> jsonResponse){
        String[] expectedValues = getExpectedUpdateValues();
        for(int i=0;i<jsonResponse.size();i++){
            Map<String,?> currentEmployee = jsonResponse;
            String[] keyList=currentEmployee.keySet().toArray(new String[currentEmployee.size()]);

            if(keyList.length!=expectedValues.length-1){
                return false;
            }
            else{ for(int j=0;j<keyList.length;j++){
                String key = keyList[j];
                if (!ArrayUtils.contains(expectedValues, key)){
                    return false;
                }
            }
            }
        }
        return true;
    }

    public String getNewEmployeeId(){
        boolean hasError=response.asString().contains("error");
        if(!hasError){
            Map<String, String> currentEmployee = response.jsonPath().get("");
            return currentEmployee.get("id");
        }
        return "";
    }
}
