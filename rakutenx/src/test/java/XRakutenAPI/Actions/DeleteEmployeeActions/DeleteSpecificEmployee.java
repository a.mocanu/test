package XRakutenAPI.Actions.DeleteEmployeeActions;

import XRakutenAPI.Actions.CreateEmployeeActions.CreateNewEmployee;
import XRakutenAPI.Actions.GetEmployeeActions.GetSpecEmployee;
import XRakutenAPI.Actions.GetEmployeeListActions.GetList;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Assert;
import static XRakutenAPI.Actions.Actions.*;

public class DeleteSpecificEmployee {

    private Response response=null;
    private CreateNewEmployee newEmployeeElement=new CreateNewEmployee();

    static String validId="";

    public Response performDeleteEmployee(String id, GetList list) {
        RestAssured.baseURI = getEndpoint();
        if(id.equals("lidl")){
            Response employeeElementList=list.performGetEmployeeList();
            if(employeeElementList.jsonPath().getList("").size() ==0){
                newEmployeeElement.performCreateEmployee("0123456789random_name0123456789","","");
                employeeElementList=list.performGetEmployeeList();
            }
            validId=list.getExistingId(employeeElementList);
        }

        if(!validId.isEmpty()){
            id=validId;
        }
        try {
            response = RestAssured.given()
                    .contentType(ContentType.JSON)
                    .delete("/delete"+"/"+id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    public String getGeneratedId(){
        return validId;
    }

    public int checkResponseCode(String itemToBeDeleted){
        boolean isIntTransformable=isInteger(itemToBeDeleted);

        if(isIntTransformable){
            Assert.assertEquals("Assert that the response code for the list of employees is 200",response.getStatusCode(),200);
            makeSomeLog("Response code for the retrieval of the employee list is 200");
        }
        else{
            Assert.assertEquals("Assert that the response code for the invalid request is 400",response.getStatusCode(),400);
            makeSomeLog("Response code for the deletion of: "+itemToBeDeleted+" is: "+response.getStatusCode()+" while expected was 400");
        }
        return response.getStatusCode();
    }

    public void checkThatItemNotInList(String itemToBeDeleted,GetSpecEmployee employee){
        Response response=employee.performGetEmployee(itemToBeDeleted);
        Assert.assertEquals("Assert that the employee no longer exists",response.asString(),"false");
    }

    //=================================================================
    //Utility methods. Methods that help the actual tests be performed.
    //=================================================================

    public String getResponseAsString(){
        String my_response=convertToString(response);
        if(response.getContentType().equals("application/json")){
            makeSomeLog("Retrieved response is in JSON format");
        }

        return my_response;
    }

    public boolean validateDeleteEmployeeId(String itemToBeDeleted){
        return isInteger(itemToBeDeleted);
    }
}
