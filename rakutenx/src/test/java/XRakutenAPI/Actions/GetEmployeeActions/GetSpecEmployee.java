package XRakutenAPI.Actions.GetEmployeeActions;

import XRakutenAPI.Actions.Actions;
import XRakutenAPI.Actions.DeleteEmployeeActions.DeleteSpecificEmployee;
import XRakutenAPI.Actions.GetEmployeeListActions.GetList;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.commons.lang3.ArrayUtils;
import org.json.simple.JSONObject;
import org.junit.Assert;
import java.util.Map;

public class GetSpecEmployee extends Actions {

    Response response=null;

    private GetList employeeList=new GetList();
    private DeleteSpecificEmployee employeeToBeDeleted= new DeleteSpecificEmployee();

    public Response performGetEmployee(String id) {
        if(!id.equals("valid") && !id.equals("invalid")) {
            RestAssured.baseURI = getEndpoint();
            try {
                response = RestAssured.given()
                       .when()
                       .get("/employee" + "/" + id);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
        else{
           return getSpecificEmployee(id);
        }
    }

    public void checkResponseCode(){
        makeSomeLog("Response code for the retrieval of the employee list is :"+response.getStatusCode()+" and the expected is: 200");
        Assert.assertEquals("Assert that the response code for the list of employees is 200",response.getStatusCode(),200);
    }

    public boolean filledInIdAndValid(){
        Map<String,String> employee = response.jsonPath().get("");
        if(employee.get("id").isEmpty() || !isInteger(employee.get("id"))){
            makeSomeLog("ID value is empty or invalid");
            return false;
        }
        return true;
    }

    public void validateResponseContent(){
       if(response.asString().equals("false")){
           Assert.assertTrue("Assert that the response contains the expected parameters",!response.asString().contains("{"));
           makeSomeLog("Employee ID not found");
       }
       else{
           boolean flag=true;
           if(getEmployeeResponse().contains("error")){
               flag=false;
           }
           else{
               Map<String, String> jsonResponse = response.jsonPath().get("");
               flag = respectsFormat(jsonResponse);}
           Assert.assertTrue("Assert that the response contains the expected parameters", flag);
           makeSomeLog("Employee has the following list of elements: name, age, salary, id, profile_image");

       }
    }

    //=================================================================
    //Utility methods. Methods that help the actual tests be performed.
    //=================================================================

    public String getEmployeeResponse(){
        String my_response=convertToString(response);
        if(my_response.equals("false")){
            makeSomeLog("ID was not found");
        }else{
            if(response.getContentType().equals("application/json")){
                makeSomeLog("Retrieved response is in JSON format");
            }
        }
        return my_response;
    }

    private Response getSpecificEmployee(String id){
        response=employeeList.performGetEmployeeList();
        String employeeId=employeeList.getExistingId(response);

        if(id.equals("invalid")){
            employeeToBeDeleted.performDeleteEmployee(employeeId,employeeList);
        }
        response=performGetEmployee(employeeId);
        return response;
    }

    private boolean respectsFormat(Map<String,?> jsonResponse){
        String[] expectedValues = getExpectedValues();
        for(int i=0;i<jsonResponse.size();i++){
            Map<String,?> currentEmployee = jsonResponse;
            String[] keyList=currentEmployee.keySet().toArray(new String[currentEmployee.size()]);

            if(keyList.length!=expectedValues.length){
                return false;
            }
            else{ for(int j=0;j<keyList.length;j++){
                String key = keyList[j];
                if (!ArrayUtils.contains(expectedValues, key)){
                    return false;
                }
            }
            }
        }
        return true;
    }
}
