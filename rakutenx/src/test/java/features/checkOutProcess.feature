Feature: Checkout payment selection

  @search_items
  Scenario Outline: Perform checkout process from product selection to payment method selection

    Given that the user is on the product page
    When the user adds the product in the cart
    And the user moves to the cart page and initiates the checkout process
    And decides to buy as a guess and move forward to the information form
    And and fills in all the requested information as "<firstName>", "<lastName>", "<Street>", "<number>", "<postalCOde>","<city>" and "<email>" and moves forward
    Then the user moved to the page that requires a payment method selection

    Examples:
     | firstName | lastName  | Street  | number  | postalCOde  | city  | email |
     | Lalala  | Xaxaxa  | Rathausstraße | 15    | 10178       | Berlin  | superoaia@yahoo.com |
