Feature: Search Result Page

  @search_items
  Scenario Outline: Search for an item and reach teh Search Results Page

    Given that the user is on the home page
    Then the user can search for an item "<item>"
    Then the user can search for an item "<item>"
    And wait for the autocompleted list to be displayed
    And select an item from the autocompleted list
    Then the user can verify that the Search Result Page is opened

    Examples:
      | item |
      | mobile |


