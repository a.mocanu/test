Feature: Login

  @login_action
  Scenario Outline: Unsuccessful login with invalid user and password

    Given that the user is on the login page
    When title of the page is Rakuten Login
    Then the user can enter "<username>" and "<password>"
    Then the user can press the login button
    And the user is login is refused

    Examples:
      | username | password |
      | adrian_agt |  adrian_agt |



  Scenario Outline: Unsuccessful login with empty user and invalid password

    Given that the user is on the login page
    When title of the page is Rakuten Login
    Then the user can enter "<username>" and "<password>"
    Then the user can press the login button
    And the user is login is refused

    Examples:
      | username | password |
      |            |  hello      |

  Scenario Outline: Unsuccessful login with empty password and invalid user

    Given that the user is on the login page
    When title of the page is Rakuten Login
    Then the user can enter "<username>" and "<password>"
    Then the user can press the login button
    And the user is login is refused

    Examples:
      | username | password |
      | iwashere   |             |

  Scenario Outline: Successful login with valid password and valid user

    Given that the user is on the login page
    When title of the page is Rakuten Login
    Then the user can enter "<username>" and "<password>"
    Then the user can press the login button
    And the user is login is accepted

    Examples:
      | username | password |
      | amocanu@pentalog.com  | Passw0rd!|