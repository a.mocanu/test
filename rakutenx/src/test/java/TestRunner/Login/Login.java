package TestRunner.Login;
import BrowserInit.Browser;
import actions.LoginActions.Loginmethods;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Login extends Browser {

    private Loginmethods Login_actions;
    @Before
    public void startSession(){
        driver= openBrowser();
        wait= initializeWait(driver);
        Login_actions=new Loginmethods();
    }

    @After
    public void CloseSession(){
        driver.close();
        driver.quit();
    }

    private By login_page=By.id("login-form");
    private By error_message=By.className("message-error");
    private By imageSelector=By.cssSelector("iframe[title='reCAPTCHA-Aufgabe']");

    @Given("^that the user is on the login page$")
    public void that_the_user_is_on_the_login_page(){
        Login_actions.moveToLoginPage();
        boolean isElementPresent = driver.findElements(login_page).size()!= 0;
        generateLog("Moving towards Login page");
        Assert.assertTrue("Login page was reached",isElementPresent);
    }

    @When("^title of the page is Rakuten Login$")
    public void title_of_the_page_is_Rakuten_Login(){
        String title= getPageTile();
        generateLog("Current page title is: "+title);
        Assert.assertEquals("Title of the page is Kundenkonto - Rakuten.de",title,"Kundenkonto - Rakuten.de");
    }

    @Then("^the user can enter \"([^\"]*)\" and \"([^\"]*)\"$")
    public void the_user_can_enter_and(String username, String password){
        Login_actions.usernameAndPassword(username,password);
        generateLog("User name: "+username+" and password: "+password+ " set in the user/password fields");
    }

    @Then("^the user can press the login button$")
    public void the_user_can_press_the_login_button(){
        Login_actions.loginButton();
        generateLog("Login button clicked");
    }

    @Then("^the user is login is refused$")
    public void the_user_not_logged_in(){
        boolean isErrorMessageDisplayed = driver.findElements(error_message).size()!= 0;
        boolean isCaptchaDisplayed = driver.findElements(imageSelector).size()!= 0;
        generateLog("Checking if credentials are valid");
        Assert.assertTrue("Error message or image selection displayed",isErrorMessageDisplayed || isCaptchaDisplayed);
    }

    @Then("^the user is login is accepted$")
    public void the_user_is_logged_in(){
        boolean isLoggedIn=Login_actions.successfulLoigin();
        generateLog("Checking if credentials are valid");
        Assert.assertTrue("User logged in",isLoggedIn);
    }

}
