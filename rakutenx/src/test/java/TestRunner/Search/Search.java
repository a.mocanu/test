package TestRunner.Search;

import BrowserInit.Browser;
import actions.LoginActions.Loginmethods;
import actions.SearchActions.Searchmethods;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Search extends Browser {

    private Searchmethods Search_actions;
    private Loginmethods pageGeneralActions;
    @Before
    public void startSession(){
        driver= openBrowser();
        wait= initializeWait(driver);
        Search_actions=new Searchmethods();
        pageGeneralActions=new Loginmethods();
    }

    @After
    public void CloseSession(){
        driver.close();
        driver.quit();
    }

    private By home_page= By.className("vw-hero");
    private By search_title=By.className("search-title");

    @Given("^that the user is on the home page$")
    public void that_the_user_is_on_the_home_page(){
        Search_actions.moveToHomePage(pageGeneralActions);
        boolean isElementPresent = driver.findElements(home_page).size()!=0;
        generateLog("Moving towards the Home page");
        Assert.assertTrue("Home page was reached",isElementPresent);
    }

    @Then("^the user can search for an item \"([^\"]*)\"$")
    public void the_user_can_search_for_an_item(String item){
        Search_actions.searchForItem(item);
    }

    @Then("^wait for the autocompleted list to be displayed$")
    public void wait_for_the_autocompleted_list_to_be_displayed(){
        boolean list=Search_actions.searchList();
        Assert.assertTrue("Search list is displayed",list);
    }

    @Then("^select an item from the autocompleted list$")
    public void select_an_item_from_the_autocompleted_list(){
        boolean result=Search_actions.selectItem();
        Assert.assertTrue("Result list displayed",result);
    }

    @Then("^the user can verify that the Search Result Page is opened$")
    public void the_user_can_verify_that_the_Search_Result_Page_is_opened() {
        Search_actions.searchResults();
        boolean isElementPresent = driver.findElements(search_title).size()!= 0;
        generateLog("Checking user current location");
        Assert.assertTrue("On the Result page",isElementPresent);
    }
}
