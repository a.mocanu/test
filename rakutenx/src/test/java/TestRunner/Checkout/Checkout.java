package TestRunner.Checkout;
import BrowserInit.Browser;
import actions.CheckoutActions.Checkoutmethods;
import actions.LoginActions.Loginmethods;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;

public class Checkout extends Browser {

    private Checkoutmethods checkout_actions;
    private Loginmethods pageGeneralActions;

    @Before
    public void startSession(){
        driver= openBrowser();
        wait= initializeWait(driver);
        checkout_actions=new Checkoutmethods();
        pageGeneralActions=new Loginmethods();
    }

    private By product_page= By.className("vw-productHeader");
    private By checkout_items=By.id("checkout-items");
    private By second_step=By.className("current-step-2");
    private By third_step=By.className("current-step-3");
    private By payment_form=By.cssSelector("form[action='/checkout/payment']");

    @After
    public void CloseSession(){
        driver.close();
        driver.quit();
    }

    @Given("^that the user is on the product page$")
    public void that_the_user_is_on_the_product_page(){
        checkout_actions.moveToProductPage(pageGeneralActions);
        boolean isElementPresent = driver.findElements(product_page).size()!= 0;
        generateLog("Checking if user is on product page");
        Assert.assertTrue("User on the product page",isElementPresent);
    }

    @When("^the user adds the product in the cart$")
    public void the_user_adds_the_product_in_the_cart(){
        int initialNumberOfItems=checkout_actions.getNumberOfItems();
        generateLog("Current number of items in the cart is: "+initialNumberOfItems);
        checkout_actions.addItemToCart();
        int newNumberOfItems=checkout_actions.getNumberOfCartItems();
        generateLog("Updated number of items in the cart is: "+newNumberOfItems);
        Assert.assertTrue("One item was added to the cart",initialNumberOfItems<newNumberOfItems);
    }

    @When("^the user moves to the cart page and initiates the checkout process$")
    public void the_user_moves_to_the_cart_page_and_initiates_the_checkout_process() {
        checkout_actions.moveToCheckout();
        boolean isElementPresent = driver.findElements(checkout_items).size()!=0;
        generateLog("Moving to checkout page");
        Assert.assertTrue("Checkout page was reached",isElementPresent);
    }

    @When("^decides to buy as a guess and move forward to the information form$")
    public void decides_to_buy_as_a_guess_and_move_forward_to_the_information_form() {
        checkout_actions.selectGuestUserType();
        checkout_actions.moveToNextStep();
        boolean isElementPresent = driver.findElements(second_step).size()!=0;
        generateLog("Reached the 2nd step of the checkout process");
        Assert.assertTrue("Second step of the checkout process was reached",isElementPresent);

    }

    @When("^and fills in all the requested information as \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\",\"([^\"]*)\" and \"([^\"]*)\" and moves forward$")
    public void and_fills_in_all_the_requested_information_as_and_and_moves_forward(String firstName, String lastName, String street, String streetNo, String postalCode, String city, String email){
        checkout_actions.moveToPaymentSelection(firstName, lastName, street, streetNo, postalCode, city, email);
        checkout_actions.moveToNextStep();
        boolean isElementPresent = driver.findElements(third_step).size()!= 0;
        generateLog("Moving towards the 3rd step of the checkout process");
        Assert.assertTrue("Third step of the checkout process was reached",isElementPresent);
    }

    @Then("^the user moved to the page that requires a payment method selection$")
    public void the_user_moved_to_the_page_that_requires_a_payment_method_selection()  {
        // Write code here that turns the phrase above into concrete actions
        boolean isElementPresent = driver.findElements(payment_form).size()!= 0;
        generateLog("Payment data step reached");
        Assert.assertTrue("Payment type selection page was reached",isElementPresent);
    }
}
