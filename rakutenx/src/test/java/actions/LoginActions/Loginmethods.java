package actions.LoginActions;

import BrowserInit.Browser;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Loginmethods extends Browser{

    public Loginmethods(){
        PageFactory.initElements(driver, this);
    }

    @FindBy(name="loginEmail")
    WebElement login_username;
    @FindBy(name="loginPassword")
    WebElement login_password;
    @FindBy(id="login-form")
     WebElement login_page;
    @FindBy(id="recaptcha-submit")
    WebElement login_button;
    @FindBy(className="privacy_prompt_content")
    WebElement cookie_popup;
    @FindBy(id="consent_prompt_submit")
    WebElement cookie_consent;
    @FindBy(className="acc-account")
    WebElement login;

    public void acceptCookies(){
        wait.until(ExpectedConditions.visibilityOf(cookie_popup));
        wait.until(ExpectedConditions.visibilityOf(cookie_consent));
        wait.until(ExpectedConditions.elementToBeClickable(cookie_consent));
        cookie_consent.click();
        generateLog("Cookies accepted");
    }

    public void moveToLoginPage(){
        Browser.moveToPage("login_page");
        wait.until(ExpectedConditions.visibilityOf(login_page));
        acceptCookies();
    }

    public void usernameAndPassword(String username, String password){
        prepareFiled(login_username,username);
        prepareFiled(login_password,password);
    }

    public void loginButton(){
        login_button.click();
    }

    public boolean successfulLoigin(){
        try {
            wait.until(ExpectedConditions.visibilityOf(login));
        } catch (Error e) {
            generateLog("User not yet logged in");
            return false;
        }
        return true;
    }

}
