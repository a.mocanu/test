package actions.SearchActions;

import BrowserInit.Browser;
import actions.LoginActions.Loginmethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class Searchmethods extends Browser {

    public Searchmethods(){
        PageFactory.initElements(driver, this);
    }

    @FindBy(className="vw-hero")
    WebElement home_page;
    @FindBy(id="search-input")
    WebElement search_field;
    @FindBy(xpath="//div[@id='eac-container-search-input']/ul")
    WebElement search_list;
    @FindBy(className="button-search")
    WebElement search_button;
    @FindBy(className="search-title")
    WebElement search_title;

    public void moveToHomePage(Loginmethods x){
        Browser.moveToPage("rakuten_search_page");
        wait.until(ExpectedConditions.visibilityOf(home_page));
        generateLog("User moved on the Home Page");
        x.acceptCookies();
    }

    public void searchForItem(String item){
        prepareFiled(search_field,item);
        wait.until(ExpectedConditions.visibilityOf(search_list));
        generateLog("Searching for: "+item);
    }

    public boolean searchList(){
        String displayedlistStyle=search_list.getAttribute("style");
        if (displayedlistStyle.endsWith("block;")){
            generateLog("List is displayed");
            return true;
        }
        else{
            generateLog("List is not displayed");
            return false;
        }
    }

    public boolean selectItem(){
        try{
            wait.until(ExpectedConditions.visibilityOf(search_list));
            driver.findElement(By.xpath("//div[@id='eac-container-search-input']/ul"+"/li[position()="+"1"+"]")).click();
            generateLog("Item selected from the list");
        }catch(Error e){
            generateLog("List was not opened");
            return false;
        }
        return true;
    }

    public void searchResults(){
        search_button.click();
        try{
            wait.until(ExpectedConditions.visibilityOf(search_title));
        }catch(Error e){
            generateLog("search title not found");
        }
    }

}
