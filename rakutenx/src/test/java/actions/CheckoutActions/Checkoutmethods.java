package actions.CheckoutActions;

import BrowserInit.Browser;
import actions.LoginActions.Loginmethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class Checkoutmethods extends Browser{


    public Checkoutmethods(){
        PageFactory.initElements(driver, this);

    }
    @FindBy(className="vw-productHeader")
    WebElement product_page;
    @FindBy(className="add-cart")
    WebElement add_to_cart;
    @FindBy(className="vw-modalAddToShopCart")
    WebElement move_to_cart_modal;
    @FindBy(xpath="//div[@class='buttons']/a[2]")
    WebElement move_to_cart;

    @FindBy(className="badge")
    WebElement cart_icon;
    @FindBy(id="go_to_checkout")
    WebElement move_to_checkout;
    @FindBy(id="go_to_next_step")
    WebElement next_step;
    @FindBy(id="login-method1")
    WebElement guest_user;

    @FindBy(className="gender-container")
    WebElement select_gender_dropdown;

    @FindBy(css="a[index='1']")
    WebElement select_gender;
    @FindBy(id="first-name")
    WebElement first_name;
    @FindBy(id="last-name")
    WebElement last_name;
    @FindBy(id="street")
    WebElement street_name;

    @FindBy(id="street-number")
    WebElement street_number;
    @FindBy(id="zip-code")
    WebElement zip_code;
    @FindBy(id="city")
    WebElement city_name;
    @FindBy(id="email")
    WebElement email_address;

    public void moveToProductPage(Loginmethods environment){
        Browser.moveToPage("product_page");
        wait.until(ExpectedConditions.visibilityOf(product_page));
        environment.acceptCookies();
    }

    public void addItemToCart(){
        wait.until(ExpectedConditions.visibilityOf(add_to_cart));
        add_to_cart.click();
        generateLog("Adding item to cart");
        moveToCart();
    }

    public int getNumberOfItems(){
        String value=cart_icon.getText();
        return Integer.parseInt(value);
    }

    public int getNumberOfCartItems(){
        String value=driver.findElement(By.className("number")).getText();
        return Integer.parseInt(value);
    }

    private void moveToCart(){
        wait.until(ExpectedConditions.visibilityOf(move_to_cart_modal));
        wait.until(ExpectedConditions.visibilityOf(move_to_cart));
        move_to_cart.click();
        generateLog("Moving to cart");
    }

    public void moveToCheckout(){
        wait.until(ExpectedConditions.visibilityOf(move_to_checkout));
        move_to_checkout.click();
        generateLog("Moving to next step of the checkout process: Checkout");
    }

    public void moveToNextStep(){
        wait.until(ExpectedConditions.visibilityOf(next_step));
        next_step.click();
        generateLog("Moving to the next step of the process");
    }

     public void selectGuestUserType(){
         wait.until(ExpectedConditions.visibilityOf(guest_user));
         guest_user.click();
         generateLog("Proceeded with guest user");
    }

    public void moveToPaymentSelection(String firstName, String lastName, String street, String streetNo, String postalCode, String city, String email){
        selectGender();
        setField(first_name,firstName);
        setField(last_name,lastName);
        setField(street_name,street);
        setField(street_number,streetNo);
        setField(zip_code,postalCode);
        setField(city_name,city);
        setField(email_address,email);
        generateLog("Required data filled in");
    }

    private void setField(WebElement selector,String field){
        wait.until(ExpectedConditions.visibilityOf(selector));
        selector.clear();
        selector.sendKeys(field);
    }

    private void selectGender(){
        wait.until(ExpectedConditions.visibilityOf(select_gender_dropdown));
        select_gender_dropdown.click();
        wait.until(ExpectedConditions.visibilityOf(select_gender));
        select_gender.click();
    }

}
