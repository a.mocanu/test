package BrowserInit;
import cucumber.api.java.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Browser {

    public static WebDriver driver;
    public static WebDriverWait wait;
    static Properties BrowserConfig= new Properties();
    private final static Logger LOGGER=Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);


    private static void getProperties() {
        try{
            BrowserConfig.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("autotest.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static WebDriver openBrowser(){
        getProperties();
        String BrowserPath=BrowserConfig.getProperty("chrome");

        System.setProperty("webdriver.chrome.driver",BrowserPath);
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();

        return driver;
    }

    public static void moveToPage(String parameter){
        String Url=BrowserConfig.getProperty(parameter);
        driver.get(Url);
        generateLog("Opened browser and moved to desired page: "+ parameter);
    }

    public static void generateLog(String message)
    {
        LOGGER.log(Level.INFO, message);
    }

    public static WebDriverWait initializeWait(WebDriver driver){
        WebDriverWait wait=new WebDriverWait(driver,120);
        return wait;
    }

    public static String getPageTile(){
        return driver.getTitle();
    }

    public void prepareFiled(WebElement field, String text){
        wait.until(ExpectedConditions.visibilityOf(field));
        wait.until(ExpectedConditions.elementToBeClickable(field));
        field.clear();
        field.sendKeys(text);
    }
}
