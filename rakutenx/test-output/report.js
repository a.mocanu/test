$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("/home/amocanu/Documents/intelij/Diabolocom/rakutenx/src/test/java/features/checkOutProcess.feature");
formatter.feature({
  "line": 1,
  "name": "Checkout payment selection",
  "description": "",
  "id": "checkout-payment-selection",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Perform checkout process from product selection to payment method selection",
  "description": "",
  "id": "checkout-payment-selection;perform-checkout-process-from-product-selection-to-payment-method-selection",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@search_items"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "that the user is on the product page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "the user adds the product in the cart",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "the user moves to the cart page and initiates the checkout process",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "decides to buy as a guess and move forward to the information form",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "and fills in all the requested information as \"\u003cfirstName\u003e\", \"\u003clastName\u003e\", \"\u003cStreet\u003e\", \"\u003cnumber\u003e\", \"\u003cpostalCOde\u003e\",\"\u003ccity\u003e\" and \"\u003cemail\u003e\" and moves forward",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "the user moved to the page that requires a payment method selection",
  "keyword": "Then "
});
formatter.examples({
  "line": 13,
  "name": "",
  "description": "",
  "id": "checkout-payment-selection;perform-checkout-process-from-product-selection-to-payment-method-selection;",
  "rows": [
    {
      "cells": [
        "firstName",
        "lastName",
        "Street",
        "number",
        "postalCOde",
        "city",
        "email"
      ],
      "line": 14,
      "id": "checkout-payment-selection;perform-checkout-process-from-product-selection-to-payment-method-selection;;1"
    },
    {
      "cells": [
        "Lalala",
        "Xaxaxa",
        "Rathausstraße",
        "15",
        "10178",
        "Berlin",
        "superoaia@yahoo.com"
      ],
      "line": 15,
      "id": "checkout-payment-selection;perform-checkout-process-from-product-selection-to-payment-method-selection;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 967142864,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Perform checkout process from product selection to payment method selection",
  "description": "",
  "id": "checkout-payment-selection;perform-checkout-process-from-product-selection-to-payment-method-selection;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@search_items"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "that the user is on the product page",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "the user adds the product in the cart",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "the user moves to the cart page and initiates the checkout process",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "decides to buy as a guess and move forward to the information form",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "and fills in all the requested information as \"Lalala\", \"Xaxaxa\", \"Rathausstraße\", \"15\", \"10178\",\"Berlin\" and \"superoaia@yahoo.com\" and moves forward",
  "matchedColumns": [
    0,
    1,
    2,
    3,
    4,
    5,
    6
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "the user moved to the page that requires a payment method selection",
  "keyword": "Then "
});
formatter.match({
  "location": "Checkout.that_the_user_is_on_the_product_page()"
});
formatter.result({
  "duration": 3198154482,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_user_adds_the_product_in_the_cart()"
});
formatter.result({
  "duration": 6385069114,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_user_moves_to_the_cart_page_and_initiates_the_checkout_process()"
});
formatter.result({
  "duration": 277395984,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.decides_to_buy_as_a_guess_and_move_forward_to_the_information_form()"
});
formatter.result({
  "duration": 4968781675,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Lalala",
      "offset": 47
    },
    {
      "val": "Xaxaxa",
      "offset": 57
    },
    {
      "val": "Rathausstraße",
      "offset": 67
    },
    {
      "val": "15",
      "offset": 84
    },
    {
      "val": "10178",
      "offset": 90
    },
    {
      "val": "Berlin",
      "offset": 98
    },
    {
      "val": "superoaia@yahoo.com",
      "offset": 111
    }
  ],
  "location": "Checkout.and_fills_in_all_the_requested_information_as_and_and_moves_forward(String,String,String,String,String,String,String)"
});
formatter.result({
  "duration": 2533831667,
  "status": "passed"
});
formatter.match({
  "location": "Checkout.the_user_moved_to_the_page_that_requires_a_payment_method_selection()"
});
formatter.result({
  "duration": 20264729,
  "status": "passed"
});
formatter.after({
  "duration": 125505398,
  "status": "passed"
});
});